﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyA : MonoBehaviour
{
    public GameObject lockedTrigger;
    public GameObject keyUI;
    public GameObject theKey;

    void OnTriggerEnter(Collider other)
    {
        keyUI.SetActive(true);
        lockedTrigger.SetActive(true);
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        theKey.SetActive(false);
    }
}
