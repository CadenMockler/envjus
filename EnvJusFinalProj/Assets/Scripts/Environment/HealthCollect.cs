﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollect : MonoBehaviour
{
    public AudioSource collectSound;

    void OnTriggerEnter(Collider other)
    {
        if (GlobalHealth.healthValue >= 81)
        {
            GlobalHealth.healthValue = 100;
        }
        else
        {
            GlobalHealth.healthValue += 20;
        }
        GetComponent<BoxCollider>().enabled = false;
        collectSound.Play();
        this.gameObject.SetActive(false);
    }
}
