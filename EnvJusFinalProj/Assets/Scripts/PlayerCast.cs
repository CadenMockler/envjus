﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCast : MonoBehaviour
{
    public static float distanceFromTarget;
    public float toTarget;

    void Update()
    {
        RaycastHit Hit;
        if (Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward), out Hit)) //Tracks distance from where looking
        {
            toTarget = Hit.distance; // Shows in debug menu now
            distanceFromTarget = toTarget;
        }
    }
}
