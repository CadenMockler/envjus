﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisibility : MonoBehaviour
{
    public GameObject invisPickup;
    public GameObject player;

    public int secondsLeft = 30;

    private GameObject enemyFoe;

    void Start()
    {
        enemyFoe = GameObject.FindGameObjectWithTag("Enemy");
       
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemyFoe.GetComponent<SoldierAI>().enabled = false;
            enemyFoe.GetComponent<LookPlayer>().enabled = false;

            invisPickup.SetActive(false);
        }
    }

    void Update()
    {
        if (invisPickup == false)
        {
            StartCoroutine(TimerTake());
        }
    }

    IEnumerator TimerTake()
    {
        yield return new WaitForSeconds(1);
        secondsLeft -= 1;
        enemyFoe.GetComponent<SoldierAI>().enabled = true;
        enemyFoe.GetComponent<LookPlayer>().enabled = true;
    }   
}
